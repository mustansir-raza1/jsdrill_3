// Iterate over each object in the array and in the email breakdown the email and return the output as below:


function emailDetails(allData){
    if(Array.isArray(allData)){
        let employeeData = allData.map((data) =>{
            let personName = data.name.split(' ');
            let personMail = data.email.split('@');

            let selectDataOfPerson = {}
            selectDataOfPerson.firstName = personName[0].toUpperCase();
            selectDataOfPerson.lastName = personName[1].toUpperCase();
            selectDataOfPerson.email = personMail[1];
            return selectDataOfPerson;

        });
        return employeeData;

    }
    else{
        return [];
    }
}
module.exports = emailDetails
