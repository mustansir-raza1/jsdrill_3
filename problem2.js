 // Check the each person projects list and group the data based on the project status and return the data as below
  
  /*
  
    {
      'Completed': ["Mobile App Redesign","Product Launch Campaign","Logo Redesign","Project A".. so on],
      'Ongoing': ["Website User Testing","Brand Awareness Campaign","Website Mockup" .. so on]
      .. so on
    }
  
  */

function groupOfProject (dataSet){
    if(Array.isArray(dataSet)){
        const groupedProjects = {}
        dataSet.forEach(person => {
            person.projects.forEach(project => {
                if (!groupedProjects[project.status]) {
                    groupedProjects[project.status] = [];
                }
                groupedProjects[project.status].push(project.name);
            });
        });
        return groupedProjects;
    }
    else{
        return [];
    }
}
module.exports = groupOfProject;

