// unique languages

function uniqueLanguages(dataSet){
    if (Array.isArray(dataSet)){
        const uniqueLang = dataSet.reduce((acc, person) => {
            person.languages.forEach(language => {
                if (!acc.includes(language)) {
                    acc.push(language);
                }
            });
            return acc;
        }, []);
        console.log(uniqueLang)
    }
    else{
        return [];
    }
}
module.exports = uniqueLanguages;
